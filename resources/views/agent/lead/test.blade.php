@extends('layouts.master')

@section('content')
<hr/>
<div class="table-responsive">
    <table id="leads" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>icon</th>
            <th>date</th>
            <th>name</th>
            <th>phone</th>
            <th>email</th>
        </tr>
        </thead>
        <tbody>
        @foreach($leads as $lead)
            <tr class="leadrow" id="{!! $lead->id !!}">
                <td></td>
                <td>{!! $lead->updated_at !!}</td>
                <td>{!! $lead->name !!}</td>
                <td>{!! $lead->phone->phone !!}</td>
                <td>{!! $lead->email !!}</td>
            </tr>
        @endforeach
        </tbody>


    </table>
    <hr/>

</div>
<div class="col-md-4">
    <div id="lead_info">
        <table id="leaddatatable" class="table table-bordered table-striped table-hover"></table>
    </div>
</div>

@endsection
