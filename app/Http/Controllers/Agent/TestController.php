<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\AgentController;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Sphere;
use App\Models\SphereMask;

class TestController extends AgentController
{
  public function index()
  {
//      $leads = $this->user->test($this->user->id)->get();
//      dd($leads);
      $leads = $this->user->test($this->user->id)->get();
      return view('agent.lead.test')->with('leads',$leads);
  }

  public function getLead($id)
  {
      $lead = $this->user->test($this->user->id)->with('phone')->with('options')->where('leads.id',$id)->first();
      $data = Sphere::findOrFail($lead->sphere_id);
      $data->load('attributes.options');
      $mask = new SphereMask($data->id,$this->uid);
      $mask = $mask->findShortMask();
      return ['lead' => $lead,'mask' => $mask];
  }
}
