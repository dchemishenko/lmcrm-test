$(function(){

	$('.leadrow').click(function() {
		var id = $(this).attr('id');
		$.ajax({
			url: "/agent/test/" + id,
			success: function(data) {

				if (!data) {
					alert('fuckup');
				} else {
					var lead = data.lead;
					var mask = data.mask;
					console.log(mask);
					$('#leaddatatable').empty();
					$('#leaddatatable').append("<tr><th>icon</th><td>" + "icon" + "</td>");
					$('#leaddatatable').append("<tr><th>date</th><td>" + lead.updated_at + "</td>");
					$('#leaddatatable').append("<tr><th>name</th><td>" + lead.name + "</td>");
					$('#leaddatatable').append("<tr><th>phone</th><td>" + lead.phone.phone + "</td>");
					$('#leaddatatable').append("<tr><th>email</th><td>" + lead.email + "</td>");
					$.each(lead.options, function (k, option) {

						$("#leaddatatable").append(function (i) {

							var cell = "<tr><th>" + option.label + "</th><td>";

							$.each(option.options, function (y,item) {
								console.log(mask[item.id]);
								var val = option.id;
								if((val == item.sphere_attr_id) && (mask[item.id] !== 0) && (mask[item.id] !== null)){
									console.log(item.name);
									cell += item.name;
								};
							});

							cell += "</td></tr>";



							return cell;

						});
					});
				}
			}
		});
	});

	if ($.isFunction($.fn.selectBoxIt)) {
	    $("select").selectBoxIt();
	}

	if ($.isFunction($.fn.datepicker)) {
		$(".datepicker").each(function (i, el) {
			var $this = $(el),
					opts = {
						format: $this.attr('format') || 'mm/dd/yyyy',
						startDate:  $this.attr('startDate') || '',
						endDate:  $this.attr( 'endDate') || '',
						daysOfWeekDisabled:  $this.attr('disabledDays') || '',
						startView:  $this.attr('startView') || 0,
					},
					$n = $this.next();
			$this.datepicker(opts);
			if ($n.is('.input-group-addon') && $n.has('a')) {
				$n.on('click', function (ev) {
					ev.preventDefault();
					$this.datepicker('show');
				});
			}
		});
	}
	if ($.isFunction($.fn.validate)) {
		$(".validate").validate();
	}

	$(".dialog").click(function(){
		var href=$(this).attr("href");
		$.ajax({
			url:href,
			success:function(response){
				var dialog = bootbox.dialog({
					message:response,
					show: false
				});
				dialog.on("show.bs.modal", function() {
					$(this).find('.ajax-form').ajaxForm(function() {
						dialog.modal('hide');
					});
				});
				dialog.modal("show");
			}
		});
		return false;
	});

	if ($.isFunction($.fn.DataTable)) {
		$('.dataTable').DataTable({
			responsive: true
		});
	}

	$('.ajax-dataTable').each(function() {
		$table=$(this);
		$container=$table.closest('.dataTables_container');
		var dTable = $table.DataTable({
			"destroy": true,
			"searching": false,
			"lengthChange": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"data": function (d) {
					var filter = {};
					$container.find(".dataTables_filter").each(function () {
						if ($(this).data('name') && $(this).data('js') != 1) {
							filter[$(this).data('name')] = $(this).val();
						}
					});
					d['filter'] = filter;
				},
			}
		});
		$container.find(".dataTables_filter").change(function () {
			if ($(this).data('js') == '1') {
				switch ($(this).data('name')) {
					case 'pageLength':
						if ($(this).val()) dTable.page.len($(this).val()).draw();
						break;
					default:
						;
				}
			} else {
				dTable.ajax.reload();
			}
		});
		$container.delegate('.ajax-link', 'click', function () {
			var href = $(this).attr('href');
			$.ajax({
				url: href,
				method: 'GET',
				success: function () {
					dTable.ajax.reload();
				}
			});
			return false;
		});
		dTable.ajax.reload();
	});
});